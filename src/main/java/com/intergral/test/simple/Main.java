/**
 * Copyright (C) 2019 Intergral Information Solutions GmbH. All Rights Reserved
 */

package com.intergral.test.simple;

import com.nerdvision.api.NerdVisionAPI;

/**
 * @author nwightma
 * @version 1.0.0
 */
public class Main
{

    public static final void main( final String args[] ) throws InterruptedException
    {
        final SimpleTest ts = new SimpleTest( "This is a test" );
        for( ;; )
        {
            try
            {
                ts.message( ts.newId() );
            }
            catch( Exception e )
            {
                NerdVisionAPI.captureException( e );
                e.printStackTrace();
                ts.reset();
            }

            Thread.sleep( 100 );
        }
    }

}
